import winsound
from time import sleep

def sonido(freq,duracion):
	winsound.Beep(freq, duracion)

print("----------------------------------------")	
print("Balada para Adelina")
print("----------------------------------------")	

#
# Obs: debio usar variables en lugar de numeros para las notas
# Muy buena melodia
# Nota: 10 de 10 (no se le baja nota por la valentia de asumir esta melodia en el trabajo, bravo!)
#
#

# Tiempos
redonda=3600
blanca=1800
negra=900
corchea=450
semicorchea=225
fusa=112
semifusa=66

sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sleep(0.40) #pausa

sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(783,semicorchea) #sol
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(783,semicorchea) #sol
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(783,semicorchea) #sol
sleep(0.40) #pausa
sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(783,semicorchea) #sol
sleep(0.40) #pausa

sonido(659,negra) #mi
sonido(659,blanca) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(880,semicorchea) #la
sonido(659,redonda) #mi
sonido(659,negra) #mi
sonido(659,negra) #mi
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(880,semicorchea) #la
sonido(659,redonda) #mi
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(587,semicorchea) #re
sonido(659,semicorchea) #mi
sonido(523,semicorchea) #do
sonido(659,semicorchea) #mi
sleep(0.40) #pausa
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si
sonido(932,semicorchea) #la#
sonido(987,semicorchea) #si
sonido(783,semicorchea) #sol
sonido(987,semicorchea) #si
#14
sonido(880,negra) #la
sonido(783,corchea) #sol
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,semicorchea) #sol
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sonido(698,semicorchea) #fa
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol

sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re

sonido(783,fusa) #sol
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re
#16
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol

sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re
sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re

sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol

sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
#17
sonido(659,negra) #mi
sonido(659,blanca) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(698,semicorchea) #fa
#20
sonido(659,negra) #mi
sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(987,semicorchea) #si
sonido(783,semicorchea) #sol

sonido(698,semicorchea) #fa
sonido(587,semicorchea) #re
sonido(880,semicorchea) #la
sonido(698,semicorchea) #fa
sonido(587,semicorchea) #re
sonido(987,semicorchea) #si
sonido(783,corchea) #sol
sonido(659,negra) #mi
sonido(659,negra) #mi
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
#22
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(698,semicorchea) #fa

sonido(659,corchea) #mi
sonido(783,corchea) #sol
sonido(587,corchea) #re
sonido(783,corchea) #sol
sonido(659,corchea) #mi
sonido(783,corchea) #sol
sonido(659,corchea) #mi
sonido(783,corchea) #sol
#25
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(587,semicorchea) #re
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sleep(0.40) #pausa
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si
sonido(932,semicorchea) #la#
sonido(987,semicorchea) #si
sonido(783,semicorchea) #sol
sonido(987,semicorchea) #si
sonido(880,negra) #la
sonido(783,corchea) #sol
sonido(698,corchea) #fa
sonido(659,fusa) #mi
sonido(659,fusa) #mi
sonido(783,fusa) #sol
sonido(659,fusa) #mi
sonido(587,fusa) #re
sonido(659,fusa) #mi
sonido(783,fusa) #sol
sonido(659,fusa) #mi
sonido(523,fusa) #do
sonido(659,fusa) #mi
sonido(783,fusa) #sol
sonido(659,fusa) #mi
sonido(987,fusa) #si
sonido(659,fusa) #mi
sonido(783,fusa) #sol
#27
sleep(0.40) #pausa
sonido(659,semicorchea) #mi 
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(587,semicorchea) #re
sonido(659,semicorchea) #mi
sonido(523,semicorchea) #do
sonido(659,semicorchea) #mi
sleep(0.40) #pausa
sonido(987,semicorchea) #si
sonido(587,semicorchea) #re
sonido(987,semicorchea) #si
sonido(932,semicorchea) #la#
sonido(987,semicorchea) #si
sonido(783,semicorchea) #sol
sonido(987,semicorchea) #si

sonido(523,negra) #do
sonido(783,corchea) #sol
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol

sonido(783,semicorchea) #sol
sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re

sonido(783,semicorchea) #sol
sonido(523,semicorchea) #do
sonido(587,semicorchea) #re
sonido(698,semicorchea) #fa

#29
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol

sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re
sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re

#30
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(523,fusa) #do
sonido(587,fusa) #re
sonido(783,fusa) #sol

sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re
sonido(587,fusa) #re
sonido(698,fusa) #fa
sonido(783,fusa) #sol
sonido(587,fusa) #re

sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol
sonido(783,fusa) #sol
sonido(987,fusa) #si
sonido(587,fusa) #re
sonido(783,fusa) #sol

#31
sonido(659,negra) #mi
sonido(659,blanca) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(698,semicorchea) #fa
#34
sonido(659,negra) #mi
sonido(783,semicorchea) #sol
sonido(659,semicorchea) #mi
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si
sonido(880,semicorchea) #la

sonido(523,semicorchea) #do
sonido(880,semicorchea) #la
sonido(698,semicorchea) #fa
sonido(587,semicorchea) #re
sonido(987,semicorchea) #si
sonido(987,semicorchea) #si

sonido(659,negra) #mi
sonido(659,negra) #mi
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
#36
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol

sonido(783,semicorchea) #sol
sonido(880,semicorchea) #la
sonido(659,corchea) #mi
sonido(783,corchea) #sol
sonido(523,corchea) #do
sonido(659,corchea) #mi

sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(880,semicorchea) #la
sonido(523,semicorchea) #do
sonido(783,semicorchea) #sol
sonido(783,semicorchea) #sol
sonido(587,semicorchea) #re
sonido(783,semicorchea) #sol
#39
sonido(659,negra) #mi
sonido(659,negra) #mi
sleep(0.40) #pausa
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(659,semicorchea) #mi
sonido(698,semicorchea) #fa
sonido(698,negra) #fa
sonido(698,negra) #fa
sleep(0.40) #pausa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(783,negra) #sol
sonido(783,blanca) #sol
sonido(783,semicorchea) #sol
sonido(880,semicorchea) #la
#42
sonido(659,blanca) #mi
sleep(0.40) #pausa
sonido(880,semicorchea) #la
sonido(523,semicorchea) #do
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(987,semicorchea) #si
sonido(587,semicorchea) #re
sonido(880,semicorchea) #la
sonido(659,blanca) #mi
sleep(0.40) #pausa
sonido(880,semicorchea) #la
sonido(523,semicorchea) #do
sonido(698,semicorchea) #fa
sonido(783,semicorchea) #sol
sonido(987,semicorchea) #si
sonido(587,semicorchea) #re
sonido(880,semicorchea) #la
sonido(659,redonda) #mi











































































































































































